const notfound=(req,res,next)=>{
    const error=new Error('not Found')
    error.status= 400;
    next(error);
}
export default notfound;