const errorHandler = (err, req, res, next) => {
  if (err.status) {
    res.status(err.status).json({ msg: err.message });
  } else {
    res.status(500).json({ msg: err.message });
  }
};
export default errorHandler;

// const errorHandler = (err, req, res, next) => {
//     const status = err.status || 500; // Default to 500 if status is not provided
//     const message = err.message || 'Internal Server Error';

//     res.status(status).json({
//       error: {
//         status: status,
//         message: message
//       }
//     });
//   };

//   export default errorHandler;