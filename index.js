// index.js

import express from 'express';
import path from 'path';
import postsRouter from './routes/posts.js'; // Adjust the path as necessary
import dotenv from 'dotenv';
import logger from'./middleware/logger.js'
import errorHandler from './middleware/error.js';
import notFound from './middleware/notFound.js';

// Load environment variables from .env file
dotenv.config();

const app = express();
const port = process.env.PORT || 8000;

// Middleware to parse JSON bodies
app.use(express.json())
app.use(express.urlencoded({extended:false}))
//   logger middleware
app.use(logger);






// Serve static files from the 'public' directory (uncomment if needed)
// app.use(express.static(path.join(__dirname, 'public')));

// Example route to serve home.html (uncomment if needed)
// app.get('/', (req, res) => {
//     res.sendFile(path.join(__dirname, 'public', 'home.html'));
// });

// Example route to serve about.html (uncomment if needed)
// app.get('/about', (req, res) => {
//     res.sendFile(path.join(__dirname, 'public', 'about.html'));
// });




// Mount the posts router at /api/posts
app.use('/api/posts', postsRouter);
//Error handler
app.use(notFound);
app.use(errorHandler)


// Start the server
app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});
