// routes/posts.js

// const express = require("express");
import express from "express";
import {getPosts,getPost,postContent,putContent,deleteContent, get} from '../controllers/postControllers.js'
const router = express.Router();


// Sample data for posts (assuming it's defined somewhere in your application)


//----------------------------------------------------------------
//! Route to get all posts with optional limit and order   http://localhost:8080/api/posts?limit=4&order=asc
router.get("/", getPosts);

//? Route to get a specific post by ID
router.get("/:id", getPost);

router.get('/',get)

//!  POST       http://localhost:8080/api/posts
router.post("/", postContent);

//!  update posts     http://localhost:8080/api/posts/1
router.put("/:id",putContent );

// Delete post
router.delete("/:id", deleteContent);

// module.exports = router;
export default router;
