let posts = [
  { id: 1, title: "Post 1", content: "Content of Post 1" },
  { id: 2, title: "Post 2", content: "Content of Post 2" },
  { id: 3, title: "Post 3", content: "Content of Post 3" },
  { id: 4, title: "Post 4", content: "Content of Post 4" },
  { id: 5, title: "Post 5", content: "Content of Post 5" },
];

 export const getPosts = (req, res, next) => {
  let { order, limit } = req.query;
  // console.log(req.query);

  // Parse and validate limit
  limit = parseInt(limit);
  if (isNaN(limit) || limit <= 0 || limit > posts.length) {
    limit = posts.length; // Default to returning all posts if limit is invalid or not provided
  }

  // Sort based on order
  if (order === "asc") {
    posts.sort((a, b) => a.id - b.id);
  } else if (order === "des") {
    posts.sort((a, b) => b.id - a.id);
  }

  // Slice posts based on limit
  let limitedPosts = posts.slice(0, limit);

  res.json({
    sortOrder: order,
    posts: limitedPosts,
  });
};

export const getPost = (req, res, next) => {
  const id = parseInt(req.params.id);
  const post = posts.find((post) => post.id === id);

  if (!post) {
    const error = new Error(`the post with id : ${id} was not found`);
    error.status = 404;
    return next(error);
  }
  res.status(200).json(post);
};

export const get = (req, res) => {
  res.json(posts);
};


export const postContent=((req,res,next) => {
    const newPost = {
      id: posts.length + 1,
      title: req.body.title,
      content: req.body.content,
    };
    if (!newPost.title) {
      const error = new Error(`please include a tittle`);
      error.status = 400;
      return next(error);
    }
    posts.push(newPost);
    res.status(201).json(posts);
  });


export const putContent=((req, res,next) => {
    const id = parseInt(req.params.id);
    const post = posts.find((post) => post.id === id);
    if (!post) {
      const error = new Error(`the post with id : ${id} was not found`);
      error.status = 404;
      return next(error);
    }
    post.title = req.body.title;
    res.status(200).json(posts);
  });

export const deleteContent=((req, res,next) => {
    const id = parseInt(req.params.id);
  
    const post = posts.find((post) => post.id === id);
  
    if (!post) {
      const error = new Error(`the post with id : ${id} was not found`);
      error.status = 404;
      return next(error);
    }
    posts = posts.filter((post) => post.id !== id);
    res.status(200).json(posts);
  })